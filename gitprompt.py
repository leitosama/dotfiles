#!/usr/bin/python
# -*- coding: utf-8 -*
import os

green  = "%{[32m%}"
red    = "%{[31m%}"
branchIcon = "⎇  "

def run(cmd):
	return os.popen(cmd).read()

def repoExists():
	return os.path.exists(".git")

def branchName():
	branchList=run("git branch")
	start_i=branchList.find('*')
	end_i=branchList.find('\n',start_i)
	return branchList[start_i+2:end_i]

def newFiles():
	status = run("git status -s")
	array=status.split('\n')
	array.pop()
	for x in array:
		if (x[1]!=' ' or x[1]=='?'):
			return red + " +"
	return ""


if repoExists():
  
  prompt = " " + green + branchIcon + branchName() + newFiles()
else:
  prompt = ""

print(prompt)