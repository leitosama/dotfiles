HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

setopt autocd extendedglob nomatch notify correctall promptsubst interactivecomments
unsetopt appendhistory beep
autoload -U colors compinit
colors 
compinit
autoload run-help-git
autoload run-help-svn

#Тут pacman, так что пока коммент
#[[ -a $(whence -p pacman-color) ]] && compdef _pacman pacman-color=pacman

#eval $(dircolors ~/.dircolors) #Прикольная идея с цветами для конкретных директорий и файлов
autoload -U pick-web-browser

alias -s {go,txt,cfg,c,cpp,rb,asm,nim,conf,d}=subl
alias -s {avi,mpeg,mpg,mov,m2v,mkv,m3u,mp3}=mpv

alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -lah --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -ah --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias cp='cp -i'
alias df='df -h'
alias free='free -hm'
alias grep='grep --colour=always'
alias uptime='uptime -p'
alias rm='rm -rf'
#alias unlock='sudo rm /var/lib/pacman/db.lck' # Pacman...

#Yeah, GIT
alias gs='git status'
alias gss='git status -s'
alias ga='git add'
alias gc='git commit -m'
alias gcam='git commit -am'
alias gp='git push origin'
alias gpl='git pull origin'
alias gd='git diff'
alias gr='git rm'
alias gl='git lg'

#Pacman(Arch)
# alias S='sudo pacman -S'
# alias Sc='echo "\n" | sudo pacman -Sc'
# alias Ss='sudo pacman -Ss'
# alias Syu='sudo pacman -Syu'
# alias Syua='yaourt -Syua'
# alias Q='sudo pacman -Q'
# alias R='sudo pacman -R'
# alias Rsc='sudo pacman -Rsc'
# alias aS='yaourt -S'
# alias aSs='yaourt -Ss'
# alias pgp='gpg --recv-key'

#Оставить как идею
#alias dotfiles='~/.dotfiles/'
#alias pycode='~/Documents/Python/'
#alias dcode='~/Documents/D/'

alias edit='subl'
#alias g='googler' #Надо будет его установить
#alias wiki='wiki-search' # arch-wiki-lite, и это тоже
#alias hdmi='xrandr --output VGA1 --auto --output HDMI1 --auto' #Идея
#alias hs='/usr/share/playonlinux/playonlinux --run "Hearthstone"' #Также идея

bindkey '\e[3~' delete-char  # del
bindkey ';5D' backward-word  # ctrl+left
bindkey ';5C' forward-word   # ctrl+right

#Идея из Archwiki
# extract <file>
ex () {
	if [ -f $1 ] ; then
    	case $1 in
    		*.tar.bz2)   tar xjf $1   ;;
    		*.tar.gz)    tar xzf $1   ;;
      		*.bz2)       bunzip2 $1   ;;
      		*.rar)       unrar x $1   ;;
      		*.gz)        gunzip $1    ;;
      		*.tar)       tar xf $1    ;;
      		*.tbz2)      tar xjf $1   ;;
      		*.tgz)       tar xzf $1   ;;
      		*.zip)       unzip $1     ;;
      		*.Z)         uncompress $1;;
      		*.7z)        7z x $1      ;;
      		*)           echo "'$1' cannot be extracted via ex()" ;;
		esac
	else
  		echo "'$1' is not a valid file"
  	fi
}

#pk <type> <file>
pk () {
	if [ $1 ] ; then
		case $1 in
			tbz)       tar cjvf $2.tar.bz2 $2      ;;
			tgz)       tar czvf $2.tar.gz  $2       ;;
			tar)      tar cpvf $2.tar  $2       ;;
			bz2)    bzip $2 ;;
			gz)        gzip -c -9 -n $2 > $2.gz ;;
			zip)       zip -r $2.zip $2   ;;
			7z)        7z a $2.7z $2    ;;
			*)         echo "'$1' не может быть упакован с помощью pk()" ;;
		esac
	else
		echo "'$1' не является допустимым файлом"
	fi
}

man() {
	env \
    	LESS_TERMCAP_md=$'\e[1;36m' \
    	LESS_TERMCAP_me=$'\e[0m' \
    	LESS_TERMCAP_se=$'\e[0m' \
    	LESS_TERMCAP_so=$'\e[1;40;92m' \
    	LESS_TERMCAP_ue=$'\e[0m' \
    	LESS_TERMCAP_us=$'\e[1;32m' \
    man "$@" 
}

local gitprompt='$(~/.dotfiles/gitprompt.py)'
export EDITOR="nano"
export PROMPT="%{$fg_bold[blue]%}% %~${gitprompt} %{$fg[white]%}% $ %{$reset_color%}%u"
clear;
